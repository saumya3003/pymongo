import pymongo

#Making a Connection with MongoClient
from pymongo import MongoClient
client = MongoClient('localhost', 27017)
# or
#client = MongoClient('mongodb://localhost:27017/')


#Getting a Database
db = client.movies

#Getting a Collection
#collection = db.test_collection

#Find the documents in the collection
cursor = db.movies.find()
for document in cursor:
    print(document)
    

#Add one document    
result = db.movies.insert_one(
    {'name': 'Padmavati', 'cost of making': '100000000'} )   

#check for the ID that got inserted
print(result.inserted_id)

#Add many document and getting the ids
result = db.movies.insert_many([{'name': 'Dhoom 4','cost of making': '900000000'},{'name': 'Golmaal 4','cost of making': '50000000'}])
print(result.inserted_ids)

#finding specific documents
cursor = db.movies.find({"name": "Padmavati"})
for document in cursor:
    print(document)

#OR operation    
cursor = db.movies.find({"$or": [{"name": "Golmaal 4"}, {"name": "Padmavati"}]})   
for document in cursor:
    print(document)
    
#Sort By name according to ASCII Value 
cursor = db.movies.find().sort('cost of making',pymongo.DESCENDING)
for document in cursor:
    print(document)
    
    
#Update

result = db.movies.update_one(
    {"name": "Golmaal 4"},
    {
        "$set": {
            "Cost_of_creation": "10000000"
        }})
    
    
result = db.movies.update_many(
    {"name": "Padmavati"},
    {
        "$set": {
            "Cost_of_creation": "67000000"
        }})  
    
print(result.matched_count)
print(result.modified_count)


#Delete a document
print(db.movies.count({"name": "Golmaal 4"}))
result = db.movies.delete_one({"name": "Golmaal 4"})
print(result.deleted_count)
print(db.movies.count({"name": "Golmaal 4"}))
    

#Delete multiple document
print(db.movies.count({"name": "Golmaal 4"}))
result = db.movies.delete_many({"name": "Golmaal 4"})
print(result.deleted_count)
print(db.movies.count({"name": "Golmaal 4"}))

#Delete all the rows
result = db.movies.delete_many({})


#drop a collection
db.movies.drop()
